package com.jb.bap.seminar.api.user;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class SignUpForm {

    @NotBlank
    @Size(min = 6, max = 50)
    private String name;

    @NotBlank
    @Email
    @Size(min = 6, max = 20)
    private String email;

    private Short role;

    @NotBlank
    private String password;
}
