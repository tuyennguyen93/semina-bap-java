/**
 * SeminarExceptionHandler.java
 */
package com.jb.bap.seminar.common.exception;

import com.jb.bap.seminar.common.response.ErrorResponse;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A class for handle global exception .
 */
@ControllerAdvice
public class SeminarExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Lỗi không tìm thấy tài nguyên.
     *
     * @param ex the exception
     * @return a ResponseEntity instance
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    protected ResponseEntity<ErrorResponse> handleResourceNotFound(ResourceNotFoundException ex) {
        ErrorResponse response = new ErrorResponse(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    /**
     * Lỗi hệ thống.
     *
     * @param ex the exception
     * @return a ResponseEntity instance
     */
    @ExceptionHandler(InternalServerException.class)
    public ResponseEntity<Object> handleInternalServerError(InternalServerException ex) {
        ErrorResponse response = new ErrorResponse(ex.getMessage(), ex.getCause().getCause().toString());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Data nhập vào conflict với data sẵn có.
     *
     * @param ex the exception
     * @return a ResponseEntity instance
     */
    @ExceptionHandler(DataConflictException.class)
    public ResponseEntity<Object> handleDataConflictException(DataConflictException ex) {
        ErrorResponse response = new ErrorResponse(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.CONFLICT);
    }

    /**
     * Xử lý paging gặp lỗi.
     *
     * @param ex the exception
     * @return a ResponseEntity instance
     */
    @ExceptionHandler(PagingException.class)
    public ResponseEntity<Object> handlePagingException(PagingException ex) {
        ErrorResponse response = new ErrorResponse(ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


    /**
     * Param input sai kiểu dữ liệu.
     */
    @Override
    protected ResponseEntity<Object> handleTypeMismatch(
            TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ErrorResponse response = new ErrorResponse("Data input wrong type", ex.getCause().toString());
        return new ResponseEntity<>(response, status);
    }

    /**
     * Lỗi khi validation data.
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        List<String> errors = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }
        ErrorResponse errorResponse = new ErrorResponse("Data validation failed", errors);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    /**
     * Lỗi input thiếu param.
     */
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        String error = ex.getParameterName() + " parameter is missing";
        ErrorResponse errorResponse = new ErrorResponse(ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    /**
     * Lỗi method không hỗ trợ.
     */
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request. Supported methods are ");
        Objects.requireNonNull(ex.getSupportedHttpMethods()).forEach(t -> builder.append(t).append(" "));

        ErrorResponse errorResponse = new ErrorResponse(
                ex.getLocalizedMessage(), builder.toString());
        return new ResponseEntity<>(
                errorResponse, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED);
    }
}
