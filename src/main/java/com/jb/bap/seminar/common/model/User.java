package com.jb.bap.seminar.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jb.bap.seminar.common.utils.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

@Entity
@Table(name = "user")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseModel {

    private String name;
    private String email;
    @JsonIgnore
    private String password;
    private Short role;
}
