package com.jb.bap.seminar.common.exception;

public class ResourceNotFoundException extends Exception {

    public ResourceNotFoundException(String msg) {
        super(msg);
    }
}