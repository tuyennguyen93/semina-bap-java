package com.jb.bap.seminar.common.exception;

public class DataConflictException extends Exception {


    public DataConflictException(String msg) {
        super(msg);
    }

    public DataConflictException(String msg, Throwable cause) {
        super(msg, cause);
    }
}