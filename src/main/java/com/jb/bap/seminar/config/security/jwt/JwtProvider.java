package com.jb.bap.seminar.config.security.jwt;

import com.jb.bap.seminar.config.security.UserPrinciple;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpiration}")
    private int jwtExpiration;

    /**
     * Create token from authentication object.
     * Set username by subject.
     */
    public String generateJwtToken(Authentication authentication) {

        UserPrinciple userPrincipal = (UserPrinciple) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpiration))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    /**
     * Get username from token
     */
    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody().getSubject();
    }

    /**
     * Validation token.
     */
    boolean validateJwtToken(String token) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature -> message: {} ", e);
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token -> message: {}", e);
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token -> message: {}", e);
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token -> message: {}", e);
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty -> message: {}", e);
        }

        return false;
    }
}
