package com.jb.bap.seminar.common.utils;

import com.jb.bap.seminar.config.security.UserPrinciple;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SeminarUtils {

    public static Integer getCurrentUser() {
        return 6;
//        try {
//            UserPrinciple principle =
//                    (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            return principle.getId();
//        } catch (Exception ex) {
//            return null;
//        }
    }

    public static List<GrantedAuthority> getCurrentUserRole() {

        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        try {
            UserPrinciple principle =
                    (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            grantedAuthorities = principle.getAuthorities();
        } catch (Exception ex) {

        }
        return grantedAuthorities;
    }
}
