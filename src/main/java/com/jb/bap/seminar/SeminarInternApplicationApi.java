package com.jb.bap.seminar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeminarInternApplicationApi {

	public static void main(String[] args) {
		SpringApplication.run(SeminarInternApplicationApi.class, args);
	}

}
