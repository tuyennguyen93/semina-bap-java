package com.jb.bap.seminar.api.user;

import com.jb.bap.seminar.common.exception.InternalServerException;
import com.jb.bap.seminar.common.model.User;
import com.jb.bap.seminar.common.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;


    public Page<User> findAll(Pageable pageRequest) {
        return userRepository.findAll(pageRequest);
    }

    public Optional<User> findById(Integer userId) {
        return userRepository.findById(userId);
    }

    public void save(SignUpForm signUpForm) {

        User user = new User(signUpForm.getName(), signUpForm.getEmail(), encoder.encode(signUpForm.getPassword()), signUpForm.getRole());
        try {
            userRepository.save(user);
        } catch (Exception e) {
            throw new InternalServerException("Cannot save user", e.getCause());
        }
    }

    public void delete(Integer userId) {
        userRepository.deleteById(userId);
    }

    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }
}
