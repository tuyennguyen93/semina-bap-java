package com.jb.bap.seminar.common.exception;

public class PagingException extends Exception {

    public PagingException(String msg) {
        super(msg);
    }

    public PagingException(String msg, Throwable cause) {
        super(msg, cause);
    }
}