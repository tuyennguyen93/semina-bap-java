package com.jb.bap.seminar.common.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.util.Strings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class ErrorResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code = Strings.EMPTY;
    private String message;
    private List<String> errors = new ArrayList<>();

    public ErrorResponse(String code, String message, List<String> errors) {
        super();
        this.code = code;
        this.message = message;
        this.errors = errors;
    }

    public ErrorResponse(String code, String message, String error) {
        super();
        this.code = code;
        this.message = message;
        errors = Arrays.asList(error);
    }

    public ErrorResponse(String message, List<String> errors) {
        super();
        this.message = message;
        this.errors = errors;
    }

    public ErrorResponse(String message, String error) {
        super();
        this.message = message;
        errors = Arrays.asList(error);
    }

    public ErrorResponse(String message) {
        this.message = message;
    }


}
