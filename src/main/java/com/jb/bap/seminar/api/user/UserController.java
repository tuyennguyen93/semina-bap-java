package com.jb.bap.seminar.api.user;

import com.jb.bap.seminar.common.exception.DataConflictException;
import com.jb.bap.seminar.common.exception.PagingException;
import com.jb.bap.seminar.common.exception.ResourceNotFoundException;
import com.jb.bap.seminar.common.repository.UserRepository;
import com.jb.bap.seminar.common.response.SuccessResponse;
import com.jb.bap.seminar.common.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * get list of users
     *
     * @return Page<User> users
     */
    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity findAll( @RequestParam(name = "page", required = false, defaultValue = "1") Integer page,
                                   @RequestParam(name = "pageSize", required = false, defaultValue = "5") Integer pageSize) throws PagingException{

        if (page < 1) {
            throw new PagingException("Page index must not be less than zero!");
        } else if (pageSize < 1){
            throw new PagingException("Page size must not be less than one!");
        }
        PageRequest request = PageRequest.of(page - 1, pageSize);
        return new ResponseEntity<>(userService.findAll(request), HttpStatus.OK);
    }

    /**
     * find user by id
     * @param userId Integer
     * @return user User
     */
    @GetMapping(value = "/{userId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public User findById(@Valid @PathVariable Integer userId) throws Exception {

        return userService.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("Student id '" + userId + "' does no exist"));
    }

    /**
     * Create new user.
     *
     * @param signUpForm {@link SignUpForm}
     * @return notice String
     */
    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity createUser(@Valid @RequestBody SignUpForm signUpForm) throws DataConflictException {

        if (userService.existsByEmail(signUpForm.getEmail())) {
            throw new DataConflictException("Email is already in use: " + signUpForm.getEmail());
        }

        userService.save(signUpForm);
        SuccessResponse response = new SuccessResponse("Successfully created new student.");

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /**
     * delete user
     *
     * @param userId Integer
     * @return notice String
     */
    @DeleteMapping(value = "/{userId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity delete(@PathVariable("userId") Integer userId) throws ResourceNotFoundException {

        SuccessResponse response;
        if (userService.findById(userId).isPresent()) {
            userService.delete(userId);
            response = new SuccessResponse("Successfully deleted student id '" + userId + "'");
        } else {
            throw new ResourceNotFoundException("Student id '" + userId + "' does no exist");
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
