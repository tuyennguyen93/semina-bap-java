package com.jb.bap.seminar.config.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jb.bap.seminar.common.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserPrinciple implements UserDetails {
    private static final long serialVersionUID = 1L;

    private Integer id;

    private String name;

    private String username;

    @JsonIgnore
    private String password;

    private List<GrantedAuthority> authorities;

    public UserPrinciple(Integer id, String name,
                         String email, String password,
                         List<GrantedAuthority> authorities) {
        this.id = id;
        this.name = name;
        this.username = email;
        this.password = password;
        this.authorities = authorities;
    }

    public static UserPrinciple build(User user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        GrantedAuthority grantedAuthority;
        if(user.getRole() == 1) {
            grantedAuthority = new SimpleGrantedAuthority("ROLE_ADMIN");
        } else {
            grantedAuthority = new SimpleGrantedAuthority("ROLE_USER");
        }
        authorities.add(grantedAuthority);

        return new UserPrinciple(
                user.getId(),
                user.getName(),
                user.getEmail(),
                user.getPassword(),
                authorities
        );
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public List<GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserPrinciple user = (UserPrinciple) o;
        return Objects.equals(id, user.id);
    }
}
