package com.jb.bap.seminar.common.exception;

public class InternalServerException extends RuntimeException {

    public InternalServerException(String msg) {
        super(msg);
    }

    public InternalServerException(String msg, Throwable cause) {
        super(msg, cause);
    }
}